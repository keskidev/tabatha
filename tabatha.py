import nmap
import time
import requests
import json
import sys
import os
import itertools
import ConfigParser
from collections import OrderedDict
from ConfigParser import RawConfigParser
from discord_webhook import DiscordWebhook
from subprocess import Popen, PIPE
from datetime import datetime
import re

def getWeatherConfig():
	scriptDir = os.path.dirname(os.path.realpath(__file__))
	configDir = os.path.join(scriptDir, "config.json")

	jsonFile = open(configDir)
	config = json.load(jsonFile)

	if len(config) < 1:
		print "length less than 1"
		sys.exit(0)

	try:
		weatherConfig = config["weather"]
	except KeyError as e:
		print "key error: " + e
		syst.exit(0)

	print weatherConfig
	if not "api_url" in weatherConfig or weatherConfig["api_url"] is None:
		print "no weather in config"
		sys.exit(0)

	return weatherConfig


def parseConfigFile():
	scriptDir = os.path.dirname(os.path.realpath(__file__))
	configDir = os.path.join(scriptDir, "config.json")

	jsonFile = open(configDir)
	config = json.load(jsonFile)

	if len(config) < 1:
		sys.exit(0)

	try:
		discordConfig = config["discord"]
		knownUsers = set()
		#print("in parse")
		for hostname, macs in config["hosts"].iteritems():
			knownUsers = [mac.upper() for mac in macs]
			#print knownUsers
			time.sleep(10)
	except KeyError as e:
		sys.exit(0)

	if not "webhook_url" in discordConfig or discordConfig["webhook_url"] is None:
		sys.exit(0)

	return discordConfig, knownUsers

def notifyDiscord(DiscordConfig, notifyMessage):
	webhook = DiscordWebhook(url=DiscordConfig["webhook_url"],content=notifyMessage)
	#print("in notify Discord ::: " + notifyMessage)

	webhook.execute()

def notifyWeather(weather_url, weather_webhook):
	r = requests.get(weather_url)
	forecast = json.loads(r.text)
	temperature = str(forecast['currently']['temperature'])

	weather_message = ""

	theTime = time.localtime()
	current_time = time.strftime("%H:%M", theTime)

	if(current_time == "7:45"):
		rain_percent = forecast['daily']['precipProbability'] * 100
		high_temp = str(forecast['daily']['temperatureHigh'])
		low_temp = str(forecast['daily']['temperatureLow'])
		chance_of_rain = str(rain_percent)

		weather_message = "Forecast: " + forecast['daily']['summary'] + "\n" + chance_of_rain + "% chance of rain. \nHigh: " + high_temp + "\nLow: " + low_temp
	else:
		temperature = str(forecast['currently']['temperature'])
		weather_message = "Currently: " + forecast['currently']['summary'] + "\nTemperature: " + temperature


	webhook = DiscordWebhook(url=weather_webhook, content=weather_message)
	webhook.execute()



def checkMACAddress(hostList, knownHosts, discordConfig, round):
	result = "";
#	print("in check mac address")
	for host in host_list:
		pid = Popen(["arp", "-n", host], stdout=PIPE)
		s = pid.communicate()[0]

		mac = re.search(r"(([a-f\d]{1,2}\:){5}[a-f\d]{1,2})",s)
		if mac:
			if(mac.group().upper() not in knownHosts):
				message = "Unknown MAC " + mac.group() + " @ IP: " + host
				print message
				notifyDiscord(discordConfig, message)
		else:
			if(host != "192.168.0.13"):
				message = "no mac found for IP: " + host
				print message
				notifyDiscord(discordConfig, message)
			else:
				print "nothing to report"
				if(round == 3):
					notifyDiscord(discordConfig, "nothing to report")
					return 0
	return round


round = 0
while True:
	
	acitiveHosts = set()
	recognizedHosts = set()
	nm = nmap.PortScanner()

	discordInfo, knownHosts = parseConfigFile()
	weatherConfig = getWeatherConfig()

	notifyWeather(weatherConfig["api_url"], weatherConfig["webhook_url"])
	#print('Known')
	#print(knownHosts)

	nm.scan(hosts=discordInfo["router"], arguments='-sP')

	host_list = nm.all_hosts()

	result = checkMACAddress(host_list, knownHosts, discordInfo, round)
	round = result

	round += 1
	time.sleep(3600)
